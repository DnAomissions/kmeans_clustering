<?php

    /**
     * RFM Scoring
     */
    function recencyScore($value)
    {
        if(0 < $value && $value < 31)
        {
            return 1;
        }
        if(31 <= $value && $value < 60)
        {
            return 2;
        }
        if($value >= 60)
        {
            return 3;
        }
    }

    function frequencyScore($value)
    {
        if(0 < $value && $value < 3)
        {
            return 1;
        }
        if(3 <= $value && $value < 6)
        {
            return 2;
        }
        if($value >= 6)
        {
            return 3;
        }
    }

    function monetaryScore($value)
    {
        if(0 < $value && $value < 500000)
        {
            return 1;
        }
        if(500000 <= $value && $value < 1500000)
        {
            return 2;
        }
        if($value >= 1500000)
        {
            return 3;
        }
    }
?>