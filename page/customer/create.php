<?php
    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Customer',
            'link' => url('/customer')
        ],
        [
            'title' => 'New Data',
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');


?>
<br>
<?php
    include 'view/form.php';
    
?>