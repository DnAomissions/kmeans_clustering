<?php
$oldPasswordCheck = false;
?>

<form class="card z-depth-3" action="<?=(($user ?? '') != '') ? url('/users/update', $user['user_id']) : url('/users/store')?>" method="post">
    <input type="hidden" name="model" value=users id="model"/>
    <div class="card-content row">
        <div class="col s12 m8 offset-m2">
            <div class="input-field" style="display:none;">
                <input name="user_id" id="user_id" value="<?= $user['user_id'] ?? ''?>" type="text" class="validate" readonly>
                <label for="user_id">User ID</label>
            </div>
            <div class="input-field">
                <input name="name" id="name" value="<?= $user['name'] ?? ''?>" type="text" class="validate" required autofocus>
                <label for="name">Name</label>
            </div>
            <div class="input-field">
                <input name="username" id="username" value="<?= $user['username'] ?? ''?>" type="text" class="validate" required>
                <label for="username">Username</label>
            </div>
            <div class="input-field">
                <select name="role" id="role" class="validate" required>
                    <option value="admin" <?=(($user['role'] ?? '') == "admin") ? 'selected' : ''?>>Admin</option>
                    <option value="user_1" <?=(($user['role'] ?? '') == "user_1") ? 'selected' : ''?>>User 1</option>
                    <option value="user_2" <?=(($user['role'] ?? '') == "user_2") ? 'selected' : ''?>>User 2</option>
                </select>
                <label for="role">Role</label>
            </div>
    <?php
        if(($user ?? '') != '' && $oldPasswordCheck)
        {
    ?>
            <div class="input-field">
                <input name="old_password" id="old_password" type="password" class="validate">
                <label for="old_password">Old Password</label>
            </div>
    <?php
        }
    ?>
            <div class="row">
                <div class="input-field col s12 m6">
                    <input name="password" id="password" type="password" class="validate" <?=(($user ?? '') != '') ? '' : 'required'?>>
                    <label for="password"><?=(($user ?? '') != '') ? 'New ' : ''?>Password <?=(($user ?? '') != '') ? '(optional)' : ''?></label>
                </div>
                <div class="input-field col s12 m6">
                    <input id="confirm_password" type="password" class="validate" <?=(($user ?? '') != '') ? '' : 'required'?>>
                    <label for="confirm_password">Confirm Password</label>
                    <span class="helper-text" data-error="Please enter confirm password correctly."></span>
                </div>
            </div>
        </div>
    </div>
    <div class="card-action row">
        <div class="col s12 m8 offset-2">
            <button id="button-submit" type="submit" name="<?=(($user ?? '') != '') ? 'update' : 'store'?>" class="btn green"><?=(($user ?? '') != '') ? 'Update' : 'Save'?></button>
        </div>
    </div>
</form>

<script>
    $(document).ready(function(){
    <?php
        if(($user ?? '') != '' && $oldPasswordCheck)
        {
    ?>
    
        function changePassword()
        {
            if($("#password").val() != '' || $("#old_password").val() != '')
            {
                $('#password').prop('required', true)
                $('#old_password').prop('required', true)
            }else{
                $('#password').prop('required', false)
                $('#old_password').prop('required', false)
            }
        }

        $("#password").on("change keyup focusout", function (e) {
            changePassword()
        });
        $("#old_password").on("change keyup focusout", function (e) {
            changePassword()
        });
    <?php
        }
    ?>

        function checkConfirmPassword()
        {
            if ($("#password").val() != $('#confirm_password').val()) {
                $('#confirm_password').removeClass("valid").addClass("invalid");
                $('#button-submit').prop('disabled', true)
            } else {
                $('#confirm_password').removeClass("invalid").addClass("valid");
                $('#button-submit').prop('disabled', false)
            }
        }
        $("#password").on("change keyup focusout", function (e) {
            checkConfirmPassword()
        });
        $("#confirm_password").on("change keyup focusout", function (e) {
            checkConfirmPassword()
        });
    });
</script>