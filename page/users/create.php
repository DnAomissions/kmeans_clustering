<?php
    // Role Permission 
    if($auth->role != 'admin')
    {
        echo "<script>window.location.replace('".url('/')."')</script>";
        exit;
    }
    
    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Users',
            'link' => url('/users')
        ],
        [
            'title' => 'New User',
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');


?>
<br>
<?php
    include 'view/form.php';
    
?>