<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Pusat Cluster</span>
            <div class="actions">
            <button class="waves-effect waves-grey green-text btn-flat table-detail-trigger" data-table="<?=$table_centeroid_id?>">Detail</button>
            </div>
        </div>
        <table class="highlight" id="<?=$table_centeroid_id?>">
            <thead>
                <tr>
                    <th>Cluster</th>
                    <?php
                    foreach($sayur as $k => $n)
                    {
                ?>
                    <th><?=$n?></th>
                <?php
                    }
                ?>
                </tr>
            </thead>
            <tbody>
        <?php
            foreach ($centeroid as $i => $value) 
            {
        ?>
                <tr>
                    <td>C<?=$i+1?></td>
                <?php
                    foreach($sayur as $k => $n)
                    {
                ?>
                    <td><?=$value[$k]?></td>
                <?php
                    }
                ?>
                </tr>
        <?php
            }
        ?>
            </tbody>
        </table>
    </div>
</div>