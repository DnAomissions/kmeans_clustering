<?php
    $model_data = new DataProduksi();

    $datas = $model_data->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Produksi',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

    // Floating Button Setup
    $button_items = [
        [
            'name' => 'Export Data',
            'icon' => 'file_download',
            'class' => 'purple',
            'link' => url('/data_produksis/export')
        ],
        [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-data_produksis'
        ],
        [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => url('/data_produksis/create')
        ]
    ];

    if(count($datas) > 0)
    {
        array_unshift($button_items, [
            'name' => 'Proses Data',
            'icon' => 'cached',
            'class' => 'orange',
            'link' => url('/data_produksis/proses_data')
        ]);
    }

    include_once load_component('floating-button');
?>
<br>
<?php
    $use_action = true;
    include 'view/table.php';

    $modal = [
        'id' => 'modal-data_produksis',
        'name' => 'Data Produksi',
        'model' => 'data_produksis',
        'text' => 'Excel Template : <a href="'.public_url('template/produksi-template.xlsx').'" download/>produksi-template.xlsx</a>',
        'action' => url('/data_produksis/import'),
    ];

    include_once load_component('modal-import');
?>