<?php
    $model_data = new Product();

    $datas = $model_data->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Products',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

    // Floating Button Setup
    $button_items = [
        [
            'name' => 'Export Data',
            'icon' => 'file_download',
            'class' => 'purple',
            'link' => url('/product/export')
        ],
        [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-product'
        ],
        [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => url('/product/create')
        ]
    ];
    include_once load_component('floating-button');
?>
<br>
<?php
    include 'view/table.php';

    $modal = [
        'id' => 'modal-product',
        'model' => 'product',
        'text' => 'Excel Template : <a href="'.public_url('template/product-template.xlsx').'" download/>product-template.xlsx</a>',
        'action' => url('/product/import'),
    ];

    include_once load_component('modal-import');
?>