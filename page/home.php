<?php
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once ROOT.'/page/components/breadcrumb.php';

    if($auth->role == 'user_1' || $auth->role == 'admin')
    {
?>
        <div class="card center-align z-depth-2" style="margin-top: 15px;">
            <div class="card-content">
                <span class="card-title">
                SISTEM INFORMASI<br>
                K-MEANS CLUSTERING UNTUK MENGELOMPOKAN PRODUKTIVITAS KECAMATAN PADA<br>
                KABUPATEN GROBOGAN<br>
                BERDASARKAN PRODUKSI HASIL TANAMAN SAYURAN
                </span>
            </div>
            <div class="card-action">
                <p>
                    Sistem Informasi ini digunakan untuk mengelompokan produktivitas kecamatan pada Kabupaten Grobogan berdasarkan produksi tanaman sayuran. Dengan adanya sistem informasi ini diharapkan bisa membantu pemerintah daerah Kabupaten Grobogan dalam mengambil beberapa kebijakan terkait dengan pengembangan produktivitas tanaman sayuran di setiap kecamatan.
                </p>
            </div>
        </div>
<?php
    }
    if($auth->role == 'user_2' || $auth->role == 'admin')
    {
?>
        <div class="card center-align z-depth-2" style="margin-top: 15px;">
            <div class="card-content">
                <span class="card-title">
                SISTEM INFORMASI<br>
                K-MEANS CLUSTERING MENGGUNAKAN MODEL REGENCY, FREQUENCY, MONETARY<br>
                (RFM)<br>
                UNTUK PENGELOMPOKAN PELANGGAN POTENSIAL
                </span>
            </div>
            <div class="card-action">
                <p>
                    Sistem Informasi ini digunakan untuk mengelompokan pelanggan potensial berdasarkan nilai Regency (kapan terakhir transaksi), Frequency (seberapa sering transaksi terjadi), dan Monetary (berapa banyak uang yang dihabiskan). Dengan adanya sistem informasi ini diharapkan bisa membantu organisasi maupun perusahaan dalam mengambil beberapa kebijakan terkait dengan pelanggannya.
                </p>
            </div>
        </div>
<?php
    }
?>