<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Pusat Cluster</span>
        </div>
        <table class="highlight">
            <thead>
                <tr>
                    <th>Cluster</th>
                    <th>Recency</th>
                    <th>Frequency</th>
                    <th>Monetary</th>
                </tr>
            </thead>
            <tbody>
        <?php
            foreach ($centeroid as $i => $value) 
            {
        ?>
                <tr>
                    <td>C<?=$i+1?></td>
                    <td><?=$value[0]?></td>
                    <td><?=$value[1]?></td>
                    <td><?=$value[2]?></td>
                </tr>
        <?php
            }
        ?>
            </tbody>
        </table>
    </div>
</div>