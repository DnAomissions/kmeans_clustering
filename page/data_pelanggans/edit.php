<?php

    $data = new DataPelanggan();
    $data = $data->find($_GET['id']);

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Pelanggan',
            'link' => url('/data_pelanggans')
        ],
        [
            'title' => $data['customer']['name'].' ('.$data['product']['name'].') / '.date('d F Y', strtotime($data['tanggal_transaksi'])),
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');


?>
<br>
<?php
    include 'view/form.php';
    
?>