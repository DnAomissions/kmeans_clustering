<?php
require_once 'Model.php';

class DataProduksi extends Model
{
    public $name = 'Data Produksi';
    public $table = 'data_produksis';
    public $primaryKey = 'data_produksi_id';
    protected $columns = ['kecamatan', 'average'];

    function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->initialize();
    }

    function initialize()
    {
        $sayur = array_values(SAYUR);
        array_push($this->columns, ...$sayur);
    }

    public function getFirstCenteroid()
    {
        $datas = $this->select();

        $averages = array_column($datas, 'average', 'data_produksi_id');

        $min = min($averages);
        $max = max($averages);
        if(count(array_unique($averages)) >= 3)
        {
            $filtered = array_values(array_filter(array_unique($averages), function($value) use ($min, $max){
                return ($value != $min && $value != $max);
            }));
            $x = $filtered[rand(0,count($filtered)-1)];
        }else{
            $x = $averages[array_rand($averages)];
        }
        $mid = $x;
        $max = $this->find(array_search($max, $averages));
        $mid = $this->find(array_search($mid, $averages));
        $min = $this->find(array_search($min, $averages));
        $rmax = [];
        $rmid = [];
        $rmin = [];
        foreach(SAYUR as $k => $s)
        {
            $rmax += [$k => $max[$k]];  
            $rmid += [$k => $mid[$k]];  
            $rmin += [$k => $min[$k]];  
        }

        $res = [$rmax, $rmid, $rmin];

        $centeroidProduksi = new CenteroidProduksi();
        $centeroidProduksi->setCenteroid($res);
        
        return $res;
    }

    public function getEuclidian($centeroid)
    {
        $datas = $this->select();

        $res = [];

        foreach($datas as $data)
        {
            $a = [];
            foreach(SAYUR as $k => $s)
            {
                $a[] = $data[$k];
            }
            $c1 = eucDistance(array_values($centeroid[0]), $a);
            $c2 = eucDistance(array_values($centeroid[1]), $a);
            $c3 = eucDistance(array_values($centeroid[2]), $a);
            
            $min = min([$c1, $c2, $c3]);
            $cluster = '';
            if($min == $c1)
            {
                $cluster = 'C1';
            }
            if($min == $c2)
            {
                $cluster = 'C2';
            }
            if($min == $c3)
            {
                $cluster = 'C3';
            }
            $arr = [
                'kecamatan' => $data['kecamatan'],
                'C1' => $c1,
                'C2' => $c2,
                'C3' => $c3,
                'cluster' => $cluster
            ];
            foreach(SAYUR as $k => $s)
            {
                $arr += [$k => $data[$k]];
            }
            $res[] = $arr;
        }

        return $res;
    }

    public function getNewCenteroid($euc)
    {
        $c1 = [];
        $c2 = [];
        $c3 = [];
        foreach ($euc as $row) {
            if($row['cluster'] === 'C1')
            {
                $c1[] = $row;
            }
            if($row['cluster'] === 'C2')
            {
                $c2[] = $row;
            }
            if($row['cluster'] === 'C3')
            {
                $c3[] = $row;
            }
        }
        
        $e1 = [];
        $e2 = [];
        $e3 = [];
        foreach(SAYUR as $k => $s)
        {
            $e1 += [$k => (count($c1) > 0) ? array_sum(array_column($c1, $k))/count($c1) : 0];
            $e2 += [$k => (count($c2) > 0) ? array_sum(array_column($c2, $k))/count($c2) : 0];
            $e3 += [$k => (count($c3) > 0) ? array_sum(array_column($c3, $k))/count($c3) : 0];
        }
        
        return [$e1,$e2,$e3];
    }

    public function create($array)
    {
        if(!array_key_exists('average', $array))
        {
            $array += ['average' => array_avg($array)];
        }

        return parent::create($array);
    }

    public function update($id, $array)
    {
        if(!array_key_exists('average', $array))
        {
            $array += ['average' => array_avg($array)];
        }
        return parent::update($id, $array);
    }

    public function truncate()
    {
        $model = new CenteroidProduksi();
        if(!$model->truncate())
        {
            return false;
        }
        return parent::truncate();
    }
}
?>