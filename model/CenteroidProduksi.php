<?php
require_once 'Model.php';

class CenteroidProduksi extends Model
{
    public $name = 'Centeroid Produksi';
    public $table = 'centeroid_produksis';
    public $primaryKey = 'centeroid_produksi_id';
    protected $columns = ['cluster'];

    function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->initialize();
    }

    function initialize()
    {
        $sayur = array_values(SAYUR);
        array_push($this->columns, ...$sayur);
    }

    public function setCenteroid($centeroid)
    {
        $this->truncate();

        foreach($centeroid as $i => $row)
        {
            $record = [
                'cluster' => 'C'.($i+1),
            ];
            foreach(SAYUR as $k => $s)
            {
                $record += [$k => $row[$k]];
            }
            $this->create($record);
        }
    }

    public function getCenteroid()
    {
        $c1 = $this->select('WHERE cluster="C1" LIMIT 1')[0];
        $c2 = $this->select('WHERE cluster="C2" LIMIT 1')[0];
        $c3 = $this->select('WHERE cluster="C3" LIMIT 1')[0];
        $r1 = [];
        $r2 = [];
        $r3 = [];

        // C1
        foreach(SAYUR as $k => $s)
        {
            $r1 += [$k => $c1[$k]];
        }

        // C2
        foreach(SAYUR as $k => $s)
        {
            $r2 += [$k => $c2[$k]];
        }
        
        // C3
        foreach(SAYUR as $k => $s)
        {
            $r3 += [$k => $c3[$k]];
        }

        $res = [$r1, $r2, $r3];

        return $res;
    }
}
?>